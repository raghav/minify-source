var gulp = require('gulp');
var concat = require('gulp-concat');
var minify = require("gulp-babel-minify");
var rename = require('gulp-rename');
let cleanCSS = require('gulp-clean-css');
let del = require('del');

var source_path = './assets';
var build_path = './build';

gulp.task('clean-js', function () {
  return del([
    'build/js/*.js'
  ]);
});

gulp.task('clean-css', function () {
  return del([
    'build/css/*.css'
  ]);
});

gulp.task("scripts", () =>
  gulp.src(source_path + "/js/**/*.js")
    .pipe(minify())
    .pipe(concat('all.js'))
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(build_path + "/js"))
);

gulp.task('styles', ['clean-css'], function () {  
  return gulp.src([ source_path + '/css/**/*.css'])
    .pipe(concat('all.css'))
    .pipe(rename({suffix: '.min'}))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest(build_path + '/css'))
});

gulp.task('watch', function() {
  gulp.watch('assets/js/**/*.js', ['scripts']);
  gulp.watch(source_path + '/css/**/*.css', ['styles']);
});

gulp.task('default', ['watch']);
